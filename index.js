console.log(`Hello world`)


/*

REPETITION CONTROL STRUCTURE

Loops
	-a way to do something repeatedly
	-for loop
	-while loop
	-do-while loop

1. For Loop
		-more flexible
		-consists of 3 parts

		1. initial value
		2. condition
		3. iteration ++ or --

	syntax:

	for(initial; condition; iteration){
		<statement or for loop codeblock
	}

	Example:
	-Go 5 steps to the east starting from 0.

	for(let x=0; x<5; x++){
		console.log(`Walking east one step:`, x);
	}

	-Loop that will count starting from 20 down to zero

	for(let x=20; x>=0; x--){
		console.log(`Skip counting by:`, x);
	}

CONTINUE AND BREAK
	
		Continue
			-skips the loop
			-proceed to the next iteration

		for(let a=1;a<20;a++){
		
			if(a%2===0){
				continue;
			}
			console.log(a);
		}


		Break
			-ends the execution of the loop

		for(let a=1;a<20;a++){
			if(a%2===0){
				continue;
			}
			
			if(a > 10){
				break;
			}
			console.log(a);
		}

LOOP IN A STRING

	-Length Property
		-returns the count of its reference variable
		-starts at 1

	-Index Property
		-in JS, index is zero based
		-referring to the position of each character or element(array)
		-starts at 0

		let myName=`Kristin`;
		console.log(myName.length);
		console.log(myName[3]);

		let i=0;
		for(i; i<14;i++){
			console.log(myName[i]);

		let i=0;
		for(i; i<myName.length;i++){
			console.log(myName[i]);

				refactor the code, character is i, continue, a, exit.

		let myName=`Kristin Ramos-Alora`;
		console.log(myName.length);
		console.log(myName[3]);

		let i=0;
		for(i; i<myName.length;i++){
			if(myName[i]=="i"){
				continue;
			}
			if(myName[i]=="a"){
				break;
			}
			console.log(myName[i]);
		}

		Outer Loop

		for(let x=0; x<=5; x++){
			console.log(x);
			
			Inner Loop

			for(let y=0;y<=5;y++)
				console.log(`${x} + ${y} = ${x+y}`);
		}

2. WHILE LOOP

		syntax:

		while(condition){
			<iteration ++ or -->
		}

		let x=1;

		while(x >=5){
			console.log(x)
			x++
		}

		let x=1;

		while(x <= 5){
			console.log(x)
			x++
		}

3. DO-WHILE LOPP
		-guaranteed do code block would run at least once before checking the condition

		syntax:

		do{
			//statement
		}while()

		let step=20;
		do{
			console.log(step)
			step--
		}while(step <= 0);

		vs

		while(step <=0){
			console.log(step)
			step--
		}


*/












